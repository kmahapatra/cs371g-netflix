#include "Netflix.hpp"

#include <iostream>  // cin, cout

int main() {
    MovieStats all_movies;
    CustomerStats all_customers;
    ActualRatings actual_ratings;

    ReadFromArchives(all_movies, all_customers, actual_ratings);
    std::cerr << "Got " << all_movies.size() << " movie stats" << std::endl;
    std::cerr << "Got " << all_customers.size() << " customer stats" << std::endl;

    NetflixSolve(std::cin, std::cout, all_movies, all_customers, actual_ratings);
    return 0;
}


