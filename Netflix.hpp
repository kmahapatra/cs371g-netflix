//
// Predict a customer rating for a movie based on training data.
// Netflix grand prize problem.
//

#ifndef Netflix_h
#define Netflix_h

#include <cmath>    // sqrt
#include <limits>   // int8_t
#include <numeric>  // transform_reduce
#include <unordered_map> // unordered_map

#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/serialization/unordered_map.hpp"

/*
 * Archive data type - MovieData
 */

struct MovieData {
    MovieData() {}
    MovieData(short movie_year)
        : year(movie_year) {}

    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* file_version */) {
        ar & year & customers_count & rating;
    }

    short year = 0;
    int customers_count = 0;
    float rating = 0;
};

/*
 * Archive data type - CustomerData
 */
struct CustomerData {
    CustomerData() {}

    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* file_version */) {
        ar & movies_count & rating;
    }

    int movies_count = 0;
    float rating = 0;
};

/*
 * Archive data types - MovieStats, CustomerStats, ActualRatings
 */
using MovieStats = std::unordered_map<short, MovieData>;
using CustomerStats = std::unordered_map<int, CustomerData>;
using CustomerRatings = std::unordered_map<int, int8_t>;
using ActualRatings = std::unordered_map<short, CustomerRatings>;

/*
 * ReadFromArchives - reads all the stats and ratings from archives
 */
void ReadFromArchives(MovieStats& all_movies,
                      CustomerStats& all_customers,
                      ActualRatings& actual_ratings);

/*
 * RmseReduce - calculate RMSE using STL algoritm
 */
template <typename Input1, typename Input2, typename T>
T RmseReduce(Input1 first1, Input1 last1, Input2 first2, T init) {
    if (first1 == last1) {
        return init;
    }
    const auto size = std::distance(first1, last1);
    //  init = std::transform_reduce(first1, last1, first2, init, plus<T>(), [] (const T& a, const T& b) -> T {const T diff = a - b; return diff * diff;});
    return sqrt(init / size);
}

template <typename II1, typename II2, typename T>
T RmseAccumulate(II1 b, II1 e, II2 c, T v) {
    if (b == e)
        return v;
    const auto s = std::distance(b, e);
    v = std::accumulate(b, e, v, [&c] (const T& v, const T& a) -> T {const T d = a - *c; ++c; return v + (d * d);});
    return sqrt(v / s);
}

/*
 * NetflixSolve reads from input and writes predictions to output
 */
void NetflixSolve(std::istream& sin, std::ostream& sout,
                  const MovieStats& all_movies,
                  const CustomerStats& all_customers,
                  const ActualRatings& actual_ratings);

/*
 * NetflixPredict - called by NetflixSolve to predict the rating for a movie/customer
 */
float NetflixPredict(int movie_id, int customer_id,
                     const MovieStats& all_movies,
                     const CustomerStats& all_customers);

/*
 * ReadMovieId, ReadCustomerId - helper functions to parse movie/customer id
 */
int ReadMovieId(const std::string& line);
int ReadCustomerId(const std::string& line);

int CountLines(const std::string& s);

#endif
