#include <cassert>  // assert
#include <cstdlib>
#include <fstream>  // ifstream, ofstream
#include <iomanip>  // setw, setfill
#include <iostream> // cout, endl
#include <string>

#include "Netflix.hpp"

static bool ReadMovieTitles(const std::string& name, MovieStats& all_movies);
static bool ReadProbe(const std::string& name, ActualRatings& actual_ratings);
static bool ReadMovieRatings(const std::string& name, int id, MovieData& movie, CustomerStats&,
                             ActualRatings&);

int main () {
    MovieStats all_movies;
    if (!ReadMovieTitles("movie_titles.txt", all_movies)) {
        return 1;
    }
    ActualRatings actual_ratings;
    if (!ReadProbe("probe.txt", actual_ratings)) {
        return 1;
    }
    CustomerStats all_customers;
    for (auto& pair : all_movies) {
        std::stringstream stream;
        stream << "training_set/mv_" << std::setw(7) << std::setfill('0') << pair.first << ".txt";
        if (!ReadMovieRatings(stream.str(), pair.first, pair.second, all_customers, actual_ratings)) {
            return 1;
        }
        MovieData& movie(pair.second);
        if (movie.customers_count > 0) {
            movie.rating /= movie.customers_count;
        }
    }
    for (auto& pair : all_customers) {
        auto& customer(pair.second);
        if (customer.movies_count > 0) {
            customer.rating /= customer.movies_count;
        }
    }
    std::cout << "Loaded " << all_movies.size() << " movies" << std::endl;
    std::cout << "Loaded " << all_customers.size() << " customers" << std::endl << std::flush;
    {
        std::ofstream ofs("kmahapatra-AllMovieStats.bin");
        boost::archive::binary_oarchive boa(ofs);
        boa << all_movies;
    }
    std::cout << "Created AllMovieStats" << std::endl << std::flush;
    {
        std::ofstream ofs("kmahapatra-AllCustomerStats.bin");
        boost::archive::binary_oarchive boa(ofs);
        boa << all_customers;
    }
    std::cout << "Created AllCustomerStats" << std::endl << std::flush;
    {
        std::ofstream ofs("kmahapatra-ActualRatings.bin");
        boost::archive::binary_oarchive boa(ofs);
        boa << actual_ratings;
    }
    std::cout << "Created ActualRatings" << std::endl << std::flush;
    return 0;
}

static bool ReadMovieTitles(const std::string& name, MovieStats& all_movies) {
    std::ifstream ifs(name);
    if (!ifs.is_open()) {
        std::cerr << "Can't open file " << name << std::endl;
        return false;
    }
    std::string line;
    char* end;
    while (std::getline(ifs, line)) {
        const char* p = line.c_str();
        int movie_id = std::strtol(p, &end, 10);
        if (movie_id < 1 || movie_id > 17770 ||  *end != ',') {
            std::cerr << "Bad movie id " << name << "[" << line << "]" << std::endl;
            return false;
        }
        p = end + 1;
        int movie_year = std::strtol(p, &end, 10);
        if (movie_year != 0 && (movie_year < 1890 || movie_year > 2005)) {
            std::cerr << "Bad movie year " << name << "[" << line << "]" << std::endl ;
            return false;
        }
        all_movies[movie_id] = MovieData(movie_year);
    }
    return true;
}

static bool ReadProbe(const std::string& name, ActualRatings& actual_ratings) {
    std::ifstream ifs(name);
    if (!ifs.is_open()) {
        std::cerr << "Can't open file " << name << std::endl;
        return false;
    }
    std::string line;
    int movie_id = 0;
    int customer_id = 0;
    int count = 0;
    ActualRatings::iterator movie_iter;
    while (std::getline(ifs, line)) {
        if (line.empty()) {
            std::cerr << "either a movie line or customer line expected in " << name << std::endl;
            return false;
        }
        std::size_t pos = 0;
        if (line.back() == ':') {
            line.pop_back();
            movie_id = std::stoi(line, &pos);
            if (movie_id == 0 || line.size() != pos) {
                std::cerr << "bad line in " << name << "[" << line << "]" << std::endl;
                return false;
            }
            auto pair_iter_bool = actual_ratings.insert(ActualRatings::value_type(movie_id, {}));
            movie_iter = pair_iter_bool.first;
        } else if (movie_id == 0) {
            std::cerr << "customer line before the movie line in " << name << "[" << line << "]" << std::endl;
            return false;
        } else {
            customer_id = std::stoi(line, &pos);
            if (customer_id == 0 || line.size() != pos) {
                std::cerr << "bad line in " << name << "[" << line << "]" << std::endl;
                return false;
            }
            movie_iter->second[customer_id] = 0;
            count++;
        }
    }
    std::cout << "Read " << count << " ratings from probe" << std::endl;
    return true;
}

static bool ReadMovieRatings(const std::string& name, int id, MovieData& movie, CustomerStats& all_customers,
                             ActualRatings& actual_ratings) {
    std::ifstream ifs(name);
    if (!ifs.is_open()) {
        std::cerr << "Can't open file " << name << std::endl;
        return false;
    }
    std::string line;
    if (!std::getline(ifs, line)) {
        std::cerr << "Can't read the file line of " << name << std::endl;
        return false;
    }
    if (line.empty() || line.back() != ':') {
        std::cerr << "The first line must end with ':' " << name << std::endl;
        return false;
    }
    line.pop_back(); // remove the trailing ':'
    std::size_t pos = 0;
    int movie_id = std::stoi(line, &pos);
    if (movie_id != id || line.size() != pos) {
        std::cerr << "The first line must have movie id " << id << " followed by : " << name << std::endl;
        return false;
    }
    char* end;
    while (std::getline(ifs, line)) {
        const char* p = line.c_str();
        int customer_id = std::strtol(p, &end, 10);
        if (customer_id < 1 || customer_id > 2649429 ||  *end != ',') {
            std::cerr << "Can't find customer id " << name << "[" << line << "]" << std::endl;
            return false;
        }
        p = end + 1;
        int rating = std::strtol(p, &end, 10);
        if (*end != ',' || rating < 1 || rating > 5) {
            std::cerr << "Invalid rating " << name << "[" << line << "]" << std::endl;
            return false;
        }
        p = end + 1;
        auto& customer = all_customers[customer_id];
        customer.movies_count++;
        customer.rating += rating;
        movie.customers_count++;
        movie.rating += rating;
        ActualRatings::iterator movie_iter = actual_ratings.find(movie_id);
        if (movie_iter != actual_ratings.end()) {
            CustomerRatings::iterator cust_iter = movie_iter->second.find(customer_id);
            if (cust_iter != movie_iter->second.end()) {
                cust_iter->second = rating;
            }
        }
    }
    return true;
}
