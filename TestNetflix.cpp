#include "gtest/gtest.h"

#include "Netflix.hpp"

TEST(NetflixFixture, read) {
    std::string s("10:");
    int movie_id = ReadMovieId(s);
    ASSERT_EQ(movie_id, 10);

    s = "250:";
    movie_id = ReadMovieId(s);
    ASSERT_EQ(movie_id, 250);

    s = "16641:";
    movie_id = ReadMovieId(s);
    ASSERT_EQ(movie_id, 16641);

    s = "3";
    int customer_id = ReadCustomerId(s);
    ASSERT_EQ(customer_id, 3);

    s = "1050";
    customer_id = ReadCustomerId(s);
    ASSERT_EQ(customer_id, 1050);

    s = "360383";
    customer_id = ReadCustomerId(s);
    ASSERT_EQ(customer_id, 360383);
}

static int CalculateRatings (std::istringstream& iss, std::ostringstream& oss) {
    MovieStats all_movies;
    CustomerStats all_customers;
    ActualRatings actual_ratings;

    ReadFromArchives(all_movies, all_customers, actual_ratings);
    NetflixSolve(iss, oss, all_movies, all_customers, actual_ratings);

    return 0;
}

TEST(NetflixFixture, lines1) {
    std::istringstream iss("59:\n2517152\n839594\n1845773\n2404115\n2536741\n2602500\n1583805\n492995\n1139393\n149838\n");
    std::ostringstream oss;

    CalculateRatings(iss, oss);

    std::string in = iss.str();
    std::string out = oss.str();

    int in_lines = CountLines(in);
    int out_lines = CountLines(out);


    ASSERT_EQ(in_lines + 1, out_lines);
}

TEST(NetflixFixture, lines2) {
    std::istringstream iss("10002:\n1450941\n4701:\n2187741\n406359\n");
    std::ostringstream oss;

    CalculateRatings(iss, oss);

    std::string in = iss.str();
    std::string out = oss.str();

    int in_lines = CountLines(in);
    int out_lines = CountLines(out);



    ASSERT_EQ(in_lines + 1, out_lines);
}

const int kLength = sizeof("RMSE: ") - 1;

TEST(NetflixFixture, rmse1) {
    std::istringstream iss("59:\n2517152\n839594\n1845773\n2404115\n2536741\n2602500\n1583805\n492995\n1139393\n149838\n");
    std::ostringstream oss;

    CalculateRatings(iss, oss);

    std::string in = iss.str();
    std::string out = oss.str();

    size_t last_line = out.find("RMSE: ");
    std::string rmse_str = out.substr(last_line + kLength);
    double rmse = std::stod(rmse_str);

    ASSERT_LT(rmse, 1.2);
}

TEST(NetflixFixture, rmse2) {
    std::istringstream iss("10002:\n1450941\n4701:\n2187741\n406359\n");
    std::ostringstream oss;

    CalculateRatings(iss, oss);

    std::string in = iss.str();
    std::string out = oss.str();

    size_t last_line = out.find("RMSE: ");
    std::string rmse_str = out.substr(last_line + kLength);
    double rmse = std::stod(rmse_str);

    ASSERT_LT(rmse, 1.2);
}


