
#include "Netflix.hpp"

#include <fstream>  // ifstream
#include <iomanip>  // setprecision
#include <iostream> // istream, ostream

void ReadFromArchives(MovieStats& all_movies,
                      CustomerStats& all_customers,
                      ActualRatings& actual_ratings) {
    {
        std::ifstream ifs("kmahapatra-AllMovieStats.bin", std::ios::binary);
        boost::archive::binary_iarchive boa(ifs);
        boa >> all_movies;
    }
    {
        std::ifstream ifs("kmahapatra-AllCustomerStats.bin", std::ios::binary);
        boost::archive::binary_iarchive boa(ifs);
        boa >> all_customers;
    }
    {
        std::ifstream ifs("kmahapatra-ActualRatings.bin", std::ios::binary);
        boost::archive::binary_iarchive boa(ifs);
        boa >> actual_ratings;
    }
}

/*

Input:

Here's an example with two movies, two IDs for the first movie, three IDs for the second movie:

2043:
1417435
2312054
10851:
1417435
2312054
462685

Output:

2043:
3.4
4.1
10851:
4.3
1.4
2.8
RMSE: 0.95

*/

void NetflixSolve(std::istream& sin, std::ostream& sout,
                  const MovieStats& all_movies,
                  const CustomerStats& all_customers,
                  const ActualRatings& actual_ratings) {
    std::string line;
    int movie_id = 0;
    std::vector<float> ratings;
    std::vector<float> predicted_ratings;
    const CustomerRatings* customer_ratings = nullptr;

    while (std::getline(sin, line)) {
        if (line.back() == ':') { // a new movie id found
            movie_id = ReadMovieId(line);
            const auto movie_iter = actual_ratings.find(movie_id);
            // Handle an unknown movie
            if (movie_iter == actual_ratings.end()) {
                customer_ratings = nullptr;
            } else {
                customer_ratings = &movie_iter->second;
            }
            sout << movie_id << ":" << std::endl;
        } else { // customer id
            assert(movie_id != 0);
            int customer_id = ReadCustomerId(line);
            float rating = NetflixPredict(movie_id, customer_id, all_movies, all_customers);
            predicted_ratings.push_back(rating);
            if (customer_ratings == nullptr) {
                // We didn't find this movie in the probe data.
                ratings.push_back(rating);
            } else {
                const auto cust_iter = customer_ratings->find(customer_id);
                if (cust_iter == customer_ratings->end()) {
                    // We didn't find this customer in the probe data.
                    ratings.push_back(rating);
                } else {
                    ratings.push_back(cust_iter->second);
                }
            }
            sout << std::setprecision(2) << rating << std::endl;
        }
    }
    const auto rmse = RmseAccumulate(ratings.begin(), ratings.end(), predicted_ratings.begin(), 0.0f);
    sout << "RMSE: " << std::setprecision(3) << rmse << std::endl;

}

int ReadMovieId(const std::string& line) {
    std::size_t pos = 0;
    int movie_id = std::stoi(line, &pos);
    assert(movie_id != 0);
    assert(line.size() - 1 == pos);
    return movie_id;
}

int ReadCustomerId(const std::string& line) {
    std::size_t pos = 0;
    int customer_id = std::stoi(line, &pos);
    assert(customer_id != 0);
    assert(line.size() == pos);
    return customer_id;
}

float NetflixPredict(int movie_id, int customer_id,
                     const MovieStats& all_movies,
                     const CustomerStats& all_customers) {
    const auto movie_iter = all_movies.find(movie_id);
    assert(movie_iter != all_movies.end());
    const auto& movie = movie_iter->second;

    const auto customer_iter = all_customers.find(customer_id);
    // This may not found
    if (customer_iter == all_customers.end()) {
        return movie.rating;
    }
    const auto& customer = customer_iter->second;
    return (movie.rating + customer.rating) / 2;
}

int CountLines(const std::string& s) {
    int count = 0;
    size_t start_pos = 0;
    size_t pos;
    while ((pos = s.find('\n', start_pos)) != std::string::npos) {
        count++;
        start_pos = pos + 1;
    }

    return count;
}

